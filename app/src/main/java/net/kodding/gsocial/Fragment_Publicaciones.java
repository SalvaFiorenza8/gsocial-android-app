package net.kodding.gsocial;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.content.ContentResolver;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by Salvatore Fiorenza on 01-06-2015.
 */
public class Fragment_Publicaciones extends Fragment {

    MenuItem fav;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_publicaciones, container, false);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.action_edit).setVisible(false);
        menu.findItem(R.id.action_group).setVisible(false);
        menu.findItem(R.id.action_send).setVisible(true);
        menu.findItem(R.id.action_add).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_send) {
            Intent myIntent = new Intent(getActivity(),CrearPublicacion.class);
            getActivity().startActivity(myIntent);
        }
        return super.onOptionsItemSelected(item);
    }
}
