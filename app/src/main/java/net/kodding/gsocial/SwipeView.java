package net.kodding.gsocial;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import android.app.ActionBar;
import android.app.FragmentTransaction;



public class SwipeView extends FragmentActivity {

    ViewPager viewPager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe_view);
        viewPager = (ViewPager) findViewById(R.id.pager);
        FragmentManager fragmentManager= getSupportFragmentManager();
        viewPager.setAdapter(new MiAdaptador(fragmentManager));
    }


}

class MiAdaptador extends FragmentPagerAdapter{


    public MiAdaptador(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position==0){
            fragment = new Fragment_Perfil();
        }
        if (position==1){
            fragment = new Fragment_Contactos();
        }
        if (position==2){
            fragment = new Fragment_Grupos();
        }
        if (position==3){
            fragment = new Fragment_Publicaciones();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
